FROM java:openjdk-8-jdk-alpine

RUN apk add --no-cache tzdata
ENV TZ America/Montevideo

RUN apk add --no-cache bash
RUN apk add --no-cache curl

RUN curl -fsSL -o /usr/bin/wait-for-it \
    https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh && \
    chmod +x /usr/bin/wait-for-it

ENV WAIT_FOR_HOST springcloudconfig-server:8888

COPY *.jar ./api.jar

ENTRYPOINT exec wait-for-it $WAIT_FOR_HOST -t 30 -- java -jar api.jar

