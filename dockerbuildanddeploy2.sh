#!/bin/bash

# Create workspace
mkdir docker-workdir && cd docker-workdir
# Download Dockerfile 
wget https://bitbucket.org/jorge-pais/bbva-apis-api-accounts/raw/71a781813d6ded960091625e69c1ff2b9f8fe6a4/Dockerfile
# Get .jar artifact
ls
cp ../rest-sb-accounts/target/*.jar ./
ls
#delete.
NEXUSURL='http://192.168.221.21:8081'
APINAME='api-test'
IMAGETAG='v1.1.1'
# Build image
docker build -rm -t ${NEXUSURL}/${APINAME} .
docker tag ${NEXUSURL}/${APINAME} ${NEXUSURL}/${APINAME}:${IMAGETAG}
# Push image to Nexus
docker login -u admin -p admin123 ${NEXUSURL}
docker push ${NEXUSURL}/${APINAME}:${IMAGETAG}
docker push ${NEXUSURL}/${APINAME}:latest
# Remove local images
docker rmi ${NEXUSURL}/${APINAME}:${IMAGETAG}
docker rmi ${NEXUSURL}/${APINAME}:latest
# Clean workspace
cd .. && rm -R docker-workdir

